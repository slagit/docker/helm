#!/bin/sh -eux

SOURCE_DATE_EPOCH=$(git log -1 --format=%at HEAD -- docker)
export SOURCE_DATE_EPOCH

buildah bud --format docker --tag helm:latest docker

digest=$(buildah images --format '{{.Digest}}' helm:latest)
if [ "$digest" != "sha256:1f58ccf602a5f665c35f6405a9604b4c9f29c42faac9e31eaa432705726e4579" ]
then
    echo "ERROR: SHA-256 hashes mismatched reproduction failed"
    false
else
    echo "Successfully reproduced helm:latest"
fi
